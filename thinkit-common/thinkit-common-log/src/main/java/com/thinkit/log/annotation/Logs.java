package com.thinkit.log.annotation;
import com.thinkit.log.enums.LogModule;
import com.thinkit.log.enums.LogOperation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Logs {

	String  operation() default "";

	LogModule module() default LogModule.DEFAULT;

	LogOperation operaEnum() default LogOperation.DEFAULT;
}
