package com.thinkit.log.service;
import com.thinkit.core.base.BaseService;
import com.thinkit.log.model.LogDto;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author lgs
 * @since 2019-08-12
 */
public interface LogService extends BaseService<LogDto> {
    /**
     * 创建操作日志记录
     * @param pjp
     * @param l
     */
    void saveLog(ProceedingJoinPoint pjp, int l);
}
