package com.thinkit.security.custom;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.handler.CustomException;
import com.thinkit.nosql.base.BaseRedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @ClassName: MyUserDetailsService
 * @Author: LG
 * @Date: 2019/3/7 11:54
 * @Version: 1.0
 **/
@Component
public class CustomUserLoginRiskCheck extends CustomRiskCheckBase {

    @Autowired
    BaseRedisService baseRedisService;

    @PostConstruct
    public void initRedis(){
       super.initRedis(baseRedisService);
    }

    public void loginRiskCheck(String userName,String clientId,boolean ckLicense) throws CustomException {
        checkMaxError(clientId+ Constants.DOT+userName);
        if(ckLicense){
            checkerLicense();
        }
    }
}
