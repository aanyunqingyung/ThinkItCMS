package com.thinkit.security.custom;
import cn.hutool.json.JSONUtil;
import com.thinkit.utils.model.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/*
 * @Author LG
 * @Description //token 认证失败统一处理即验证jwt是否合法
 * @Date 10:33 2019/4/30
 * @Param
 * @return
 **/
@Slf4j
@Component
public class CustomAuthenDeniedEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
		log.error("-----------jwt 请求登录失效--------------"+request.getRequestURI());
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter writer = response.getWriter();
		writer.write(JSONUtil.toJsonStr(ApiResult.result(HttpStatus.UNAUTHORIZED.value(),"授权失效,请重新登录!")));
	}
}
