package com.thinkit.core.annotation;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.enums.SiteOperation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {

	ActionEnum action() default ActionEnum.CREATE_CONTENT;

}
