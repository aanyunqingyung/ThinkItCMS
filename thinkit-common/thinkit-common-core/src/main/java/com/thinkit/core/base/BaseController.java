package com.thinkit.core.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

public class BaseController<Service extends BaseService>{

    @Autowired
    protected Service service;

	@RequestMapping("delByPk")
    protected boolean delByPk(@NotBlank @RequestParam String pk){
        return service.deleteByPk(pk);
    }

    @RequestMapping("getByPk")
    protected Object getByPk(@NotBlank(message = "主键不能为空")  String pk){
        return service.getByPk(pk);
    }

    @GetMapping("loadSystemInfo")
    protected Map loadSystemInfo(){
        Map<String,Object> params = new HashMap<>();
        params.put("Version","2.0");
        params.put("Boot","SpringBoot");
        params.put("Template","Freemarker");
        params.put("Vue","Ant Design vue");
        params.put("Sign","Think-It-Cms");
        return params;
    }

    protected  String getUserId(){
	    return BaseContextKit.getUserId();
    }

    protected  String getOrgId(){
        return BaseContextKit.getOrgId();
    }

}