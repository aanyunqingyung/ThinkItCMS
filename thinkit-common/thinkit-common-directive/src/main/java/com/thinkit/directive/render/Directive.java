package com.thinkit.directive.render;

import com.thinkit.directive.emums.DirectiveEnum;

/**
 * 
 * Directive 指令接口
 *
 */
public interface Directive {

    /**
     * @throws Exception
     */
     void execute(BaserRender render) throws  Exception;

    /**
     * 指定指令名称
     * @return
     */
     DirectiveEnum getName();
}
