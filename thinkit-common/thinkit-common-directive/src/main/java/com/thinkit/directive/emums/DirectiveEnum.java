package com.thinkit.directive.emums;

import lombok.Getter;

public enum DirectiveEnum {

	NAVBAR("navbar","bars","获取栏目数"),

	SITE("site","site","获取站点信息"),

	FRAGMENT_DATA("fdata","datas","获取页面片段数据"),

	CATEGORY_DATA_BY_CODE_DIRECTIVE("cdata","contents","根据栏目编码获取栏目内容"),

	UP_TO_DATE_DIRECTIVE("uptoDate","contents","获取最新发布的内容"),

	CMS_CATEGORY_DIRECTIVE("category","category","获取栏目详情"),

	CMS_CONTENT_INFO_DIRECTIVE("content","content","获取内容详情"),

	CMS_CATEGORY_SINGLE_DATA_DIRECTIVE("singleData","content","获取单页栏目内容"),

	CMS_CATEGORY_CONTENG_NEXTPREVIOUS("nextPrevious","content","获取分类内容[code]"),

	BREADCRUMB_DIRECTIVE("breadCrumb",	"categorys","获取面包屑导航"),

	CMS_TOPNEW_DIRECTIVE("topNew","contents","获取指定栏目的最新内容"),

	CMS_TOPTGS_DIRECTIVE("topTags","tags","获取置顶标签的文章列表")

	//CMS_TEST("test","test","test")
	;


	@Getter
	private String value;

	@Getter
	private String code;

	@Getter
	private String name;

	@Getter
	private String sourceTarget;


	DirectiveEnum(String value, String code, String name) {
		this.name = name;
		this.value = value;
		this.code = code;
	}

	DirectiveEnum(String value, String code, String name, String sourceTarget) {
		this.name = name;
		this.value = value;
		this.code = code;
		this.sourceTarget=sourceTarget;
	}
}
