package com.thinkit.directive.components;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;


/**
 * 通知组件
 */
@Slf4j
@Component
public class NotifyComponent extends AbstractNotify{


    @Autowired
    SimpMessagingTemplate SMT;

    @Override
    public void notifyMsg(String principal, ApiResult result) {
        if(Checker.BeNotNull(result)){
            SMT.convertAndSendToUser(principal,"/queue/msg",result.getObj());
        }
    }
}
