package com.thinkit.utils.enums;
import lombok.Getter;

public enum ActionEnum {
	CREATE_CONTENT("CREATE_CONTENT","创建内容"),
	UPDATE_CONTENT("UPDATE_CONTENT","修改内容"),
	TOP_CONTENT("TOP_CONTENT","置顶内容"),
	PUBLISH_CONTENT("PUBLISH_CONTENT","发布内容"),
	DELETE_CONTENT("DELETE_CONTENT","删除内容"),
	CREATE_CATEGORY("CREATE_CATEGORY","创建栏目"),
	UPDATE_CATEGORY("UPDATE_CATEGORY","修改栏目"),
	DELETE_CATEGORY("DELETE_CATEGORY","删除栏目"),
	GENERATE_CATEGORY("GENERATE_CATEGORY","栏目静态化"),
	BATCH_GENERATE("BATCH_GENERATE","批量生成静态页"),
	BATCH_MOVE("BATCH_MOVE","批量移动");

	@Getter
	private String code;

	@Getter
	private String name;

	ActionEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public static ActionEnum getActionEnum(String code) {
		for (ActionEnum each : ActionEnum.class.getEnumConstants()) {
			if (code.equals(each.code)) {
				return each;
			}
		}
		return null;
	}

	public static String getActionName(String code) {
		return getActionEnum(code).getName();
	}
}
