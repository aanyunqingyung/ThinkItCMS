package com.thinkit.processor.message;

import cn.hutool.core.date.DateUtil;
import com.thinkit.core.constant.Channel;
import com.thinkit.core.constant.Constants;
import com.thinkit.processor.channel.BaseChannelService;
import com.thinkit.processor.license.LicenseProperties;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.MessageListener;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
@Slf4j
public abstract class MessageHandler implements MessageListener,MessageActuator {

    @Autowired
    public ThinkItProperties thinkItProperties;

    @Autowired
    public LicenseProperties licenseProperties;

    private ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

    protected Map<String, BaseChannelService> channelServiceMap =new HashMap<>();


    public void submit(MessageResolve messageResolve){
        messageResolve.channelService(channelServiceMap.get(messageResolve.channel()));
        cachedThreadPool.submit(messageResolve);
    }


    protected boolean verifyLicense(Object msg){
        if(msg instanceof Map){
            Map mapMsg = (Map) msg;
            String domian = (mapMsg.containsKey(Channel.DOMAIN) && Checker.BeNotNull(mapMsg.get(Channel.DOMAIN))) ?
            mapMsg.get(Channel.DOMAIN).toString():null;
            if(Checker.BeNotBlank(domian)){
                if(licenseProperties.needVerifyDomain()){
                    boolean verifyRes= verifyDomain(domian);
                    if(!verifyRes){
                        return false;
                    }
                }
                if(licenseProperties.needVerifyDate()){
                    boolean verifyRes= verifyDate();
                    if(!verifyRes){
                        return false;
                    }
                }
                boolean legal = licenseProperties.verify();
                if(!legal){
                    log.error("证书不合法!");
                    return legal;
                }
            }
        }
        return true;
    }

    private boolean verifyDomain( String domian){
        List<String> domains = Arrays.asList(licenseProperties.getDomain().split("\\|"));
        return domains.contains(domian);
    }

    private boolean verifyDate(){
        Date now = DateUtil.date(),start=null,end=null;
        try {
            String[] startEnd=licenseProperties.getStartStopTime().split("~");
            start = DateUtil.parse(startEnd[0]);
            end = DateUtil.parse(startEnd[1]);
            int isSatrt=DateUtil.compare(now,start);
            int isEnd=DateUtil.compare(now,end);
            if(isSatrt==-1){
                log.error("license证书授权未生效");
                return false;
            }
            if(isEnd==1){
                log.error("license证书授权已过期");
                return false;
            }
        }catch (Exception e){
            log.error("证书日期不合法!");
            return false;
        }
        return true;
    }

}
