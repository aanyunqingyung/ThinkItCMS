package com.thinkit.processor.license;

import cn.hutool.core.io.FileUtil;
import com.thinkit.processor.annotation.LicenseMark;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LicenseProperties {

    @Autowired
    private ThinkItProperties thinkItProperties;


    public boolean needVerifyDomain(){
        return !("*".equals(domain));
    }

    public boolean needVerifyDate(){
        return !("*".equals(startStopTime));
    }

    public boolean verify(){
        try {
            return RSAUtils.verify(toString().getBytes(), publicKey, signaturer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @PostConstruct
    public void init(){
        boolean licenseExist = FileUtil.exist(thinkItProperties.getLicense());
        if(licenseExist){
            File file = new File(thinkItProperties.getLicense());
            List<String> properties=new ArrayList();
            properties=FileUtil.readUtf8Lines(file,properties);
            if(Checker.BeNotEmpty(properties)){
                Map<String,String> propertieMap = new HashMap<>();
                for(String propertie:properties){
                    String[] parr = propertie.split("\\:");
                    propertieMap.put(parr[0],parr[1]);
                }
                buildProperties(propertieMap);
            }
        }
    }

    private void buildProperties(Map<String,String> propertieMap){
        Field[] fields=this.getClass().getDeclaredFields();
        if(Checker.BeNotEmpty(fields)){
            for(Field field:fields){
                field.setAccessible(true);
                if(field.getAnnotation(LicenseMark.class)!=null){
                    String value =propertieMap.get(field.getName());
                    try {
                        field.set(this,value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Getter
    @LicenseMark
    private String version;

    @Getter
    @LicenseMark
    private String  organization;

    @Getter
    @LicenseMark
    private String domain;

    @Getter
    @LicenseMark
    private String startStopTime;

    @Getter
    @LicenseMark
    private String authorizeDesc ;

    @Getter
    @LicenseMark
    private String copyrightOwner;

    @Getter
    @LicenseMark
    private String signaturer;

    @Getter @Setter
    private String status;

    private String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCkD7vzjM6IFj9dBftSbf0piS9A9mLW8YnGAV4Rdy1Z3P0wJ+lGnBDWNWlKX7cRtHsGsodar4vJelgOjQWNMDqlT1ngJEbz3EbJlwem7d0e59Bww1HF+DuPBM1/+jpspjk5zy1bgLmSrH8tJ5LX/O0t4A+BtnvZPUZKCBytAbxJlwIDAQAB";

    @Override
    public String toString() {
        return "version="+version+",organization="+organization+",domain="+domain+",startStopTime="+startStopTime+",authorizeDesc="+authorizeDesc+",copyrightOwner="+copyrightOwner;
    }
}
