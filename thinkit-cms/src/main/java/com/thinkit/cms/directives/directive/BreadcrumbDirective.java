package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * 面包屑导航
 */
@Component
public class BreadcrumbDirective extends BaseDirective {

    @Autowired
    CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String id=render.getString("categoryId"); // 栏目ID
        String postfix=render.getString("postfix"); // 面包 分隔符
        Boolean containit=render.getBoolean("containit"); // 包含本栏目
        if(Checker.BeNotBlank(id)){
            if(Checker.BeBlank(postfix)){
                postfix = "/";
            }
            if(Checker.BeNull(containit)){
                containit = true;
            }
            List<CategoryDto>  categorys = categoryService.breadCrumbs(id,postfix,containit);
            if(Checker.BeNotEmpty(categorys)){
                Collections.reverse(categorys);
            }
            render.put(getName().getCode(),categorys).render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.BREADCRUMB_DIRECTIVE;
    }
}
