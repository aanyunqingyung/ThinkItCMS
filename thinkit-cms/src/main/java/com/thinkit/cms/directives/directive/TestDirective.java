//package com.thinkit.cms.directives.directive;
//import com.thinkit.cms.api.content.ContentService;
//import com.thinkit.directive.emums.DirectiveEnum;
//import com.thinkit.directive.render.BaseDirective;
//import com.thinkit.directive.render.BaserRender;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import java.io.IOException;
//
//@Component
//public class TestDirective extends BaseDirective {
//
//    @Autowired
//    ContentService contentService;
//
//    @Override
//    public void execute(BaserRender render) throws IOException, Exception {
//        Integer categoryId1=render.getInteger("categoryId");// 分类id
//        Double categoryId23=render.getDouble("categoryId");// 分类id
//        String[] paramsss = render.getStringArray("cid");
//        render.put("categoryId1",categoryId1).put("test","22").render();
//    }
//
//    @Override
//    public DirectiveEnum getName() {
//        return DirectiveEnum.CMS_TEST;
//    }
//}
