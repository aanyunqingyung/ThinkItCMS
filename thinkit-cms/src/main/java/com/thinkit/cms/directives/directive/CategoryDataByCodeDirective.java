package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class CategoryDataByCodeDirective extends BaseDirective {

    @Autowired
    ContentService contentService;

    @Autowired
    CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String code=render.getString("code");
        String categoryId=render.getString("categoryId");
        Integer num=render.getInteger("num");
        String order=render.getString("order");
        if(Checker.BeNotBlank(code) || Checker.BeNotBlank(categoryId)){
            num = Checker.BeNotNull(num)?num:categoryService.queryPageSize(code,categoryId);
            List<Map<String,Object>> contents = contentService.listByCode(code,categoryId,num,order);
            render.put(getName().getCode(),contents).render();
        }
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.CATEGORY_DATA_BY_CODE_DIRECTIVE;
    }
}
