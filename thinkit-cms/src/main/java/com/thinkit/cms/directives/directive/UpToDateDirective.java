package com.thinkit.cms.directives.directive;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.directive.emums.DirectiveEnum;
import com.thinkit.directive.render.BaseDirective;
import com.thinkit.directive.render.BaserRender;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 获取最新的内容指令
 */
@Component
public class UpToDateDirective extends BaseDirective {

    @Autowired
    ContentService contentService;

    @Autowired
    CategoryService categoryService;

    @Override
    public void execute(BaserRender render) throws IOException, Exception {
        String code=render.getString("code");
        String categoryId=render.getString("categoryId");
        Integer num=render.getInteger("num",20);
        String order=render.getString("order");
        Boolean publish=render.getBoolean("publish",true);
        num = Checker.BeNotNull(num)?num:categoryService.queryPageSize(code,categoryId);
        List<Map<String,Object>> contents = contentService.listUpToDate(code,categoryId,num,order,publish);
        render.put(getName().getCode(),contents).render();
    }

    @Override
    public DirectiveEnum getName() {
        return DirectiveEnum.UP_TO_DATE_DIRECTIVE;
    }
}
