package com.thinkit.cms.controller.channel;

import com.thinkit.cms.api.category.ChannelNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 分类 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
@Validated
@RestController
@RequestMapping("channelNotify")
public class ChannelNotifyController  {

    @Autowired
    ChannelNotifyService channelNotifyService;

    /**
     * 手动生成首页
     */
    @PutMapping(value="notifyHomePage")
    public void notifyHomePage(){
        channelNotifyService.notifyHomePage();
    }

    /**
     * 手动生成全站
     */
    @PutMapping(value="notifyWholeSite")
    public void notifyWholeSite(){
        channelNotifyService.notifyWholeSite();
    }

    /**
     * 手动生成全站
     */
    @PutMapping(value="notifyDbToNoSql")
    public void notifyDbToNoSql(){
        channelNotifyService.notifyDbToNoSql(1,1000,1);
    }

    /**
     * 清除缓存
     */
    @PutMapping(value="flushdb")
    public void flushdb(){
        channelNotifyService.flushdb();
    }



}
