package com.thinkit.cms.controller.resource;

import com.thinkit.cms.api.resource.SysFileGroupService;
import com.thinkit.cms.dto.resource.SysFileGroupDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.core.handler.CustomException;
import com.thinkit.utils.model.ApiResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sysFileGroup
 * @since 2021-07-03
 */
@Validated
@RestController
@RequestMapping("sysFileGroup")
public class SysFileGroupController extends BaseController<SysFileGroupService> {



    @SiteMark
    @PostMapping("list")
    public  List<SysFileGroupDto> list(@RequestBody SysFileGroupDto v){
        return service.listGroupName(v);
    }

    @SiteMark
    @PostMapping("addGroup")
    public ApiResult addGroup(@Validated @RequestBody SysFileGroupDto v){
         return  service.addGroup(v);
    }

    @DeleteMapping("deleteGroup")
    public ApiResult addGroup(@NotBlank(message = "主键不能为空") String id){
        return  service.deleteGroup(id);
    }



    @PostMapping("updateFileGid")
    public ApiResult updateFileGid(@NotBlank(message = "fileId不能为空") String fileId,String gid){
        return  service.updateFileGid(fileId,gid);
    }

    @SiteMark
    @PostMapping("renameGroup")
    public  void renameGroup(@RequestBody SysFileGroupDto v){
         service.renameGroup(v);
    }


}
