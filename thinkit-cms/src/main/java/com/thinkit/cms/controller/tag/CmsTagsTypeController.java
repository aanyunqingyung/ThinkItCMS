package com.thinkit.cms.controller.tag;
import com.thinkit.cms.api.tag.CmsTagsTypeService;
import com.thinkit.cms.dto.tag.CmsTagsTypeDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.PageDto;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 标签类型 前端控制器
 * </p>
 *
 * @author LG
 * @since 2020-01-31
 */
@Validated
@RestController
@RequestMapping("tagsType")
public class CmsTagsTypeController extends BaseController<CmsTagsTypeService> {


    @GetMapping("getByPk")
    public CmsTagsTypeDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @SiteMark
    @Logs(module = LogModule.TAG,operation = "创建标签分类")
    @PostMapping(value="save")
    public void save(@Validated @RequestBody CmsTagsTypeDto v){
        service.save(v);
    }


    @Logs(module = LogModule.TAG,operation = "更新标签分类")
    @PutMapping("update")
    public void update(@RequestBody CmsTagsTypeDto v){
        service.update(v);
    }


    @Logs(module = LogModule.TAG,operation = "删除标签分类")
    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteById(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @SiteMark
    @PostMapping("list")
    public  List<CmsTagsTypeDto> list(@RequestBody CmsTagsTypeDto v){
        return service.listDto(v);
    }

    @SiteMark
    @Logs(module = LogModule.TAG,operation = "查看标签分类列表页")
    @PostMapping("page")
    public PageDto<CmsTagsTypeDto> listPage(@RequestBody PageDto<CmsTagsTypeDto> pageDto){
        return service.listPage(pageDto);
    }

    @Logs(module = LogModule.TAG,operation = "归属标签")
    @PutMapping("tagsBelong")
    public void tagsBelong(@RequestBody CmsTagsTypeDto v){
        service.tagsBelong(v);
    }

}
