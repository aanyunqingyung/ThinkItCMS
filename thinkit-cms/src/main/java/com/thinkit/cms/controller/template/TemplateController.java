package com.thinkit.cms.controller.template;

import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.dto.template.TemplateContentDto;
import com.thinkit.cms.dto.template.TemplateDto;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.enums.SiteOperation;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.TreeFileModel;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 模板表 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
@Validated
@RestController
@RequestMapping("template")
public class TemplateController extends BaseController<TemplateService> {


    @GetMapping("getByPk")
    public TemplateDto get(@NotBlank @RequestParam String id){
       return  service.getByPk(id);
    }

    @GetMapping("checkerIsStartTemplate")
    public ApiResult checkerIsStartTemplate(@NotBlank @RequestParam String templateId){
        return  service.checkerIsStartTemplate(templateId);
    }



    @PostMapping(value="save")
    public void save(@Validated @RequestBody TemplateDto v){
        service.save(v);
    }

    @PutMapping("update")
    public void update(@RequestBody TemplateDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteByPk(id);
    }

    @DeleteMapping(value = "deleteByIds")
    public void deleteByPks(@NotEmpty @RequestBody List<String> ids){
          service.deleteByPks(ids);
    }

    @PostMapping("list")
    public  List<TemplateDto> list(@RequestBody TemplateDto v){
        return service.listDto(v);
    }


    @SiteMark(type = SiteOperation.READ)
    @PostMapping("page")
    public PageDto<TemplateDto> listPage(@RequestBody PageDto<TemplateDto> pageDto){
        return service.listPage(pageDto);
    }

    @GetMapping("loadTemplateTree")
    public List<TreeFileModel> loadTemplateTree(@RequestParam String id){
        return service.loadTemplateTree(id);
    }

    @GetMapping("loadPartFile")
    public List<TreeFileModel> loadPartFile(@RequestParam String id){
        return service.loadPartFile(id);
    }

    @GetMapping("openFile")
    public void openFile(@NotBlank @RequestParam String path){
        if(FileUtil.exist(path)){
            try {
                Desktop.getDesktop().open(new File(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @PutMapping("createFile")
    public boolean createFile(@RequestParam String path,@RequestParam String fileName,@RequestParam Integer type){
        return service.createFile(path,fileName,type);
    }

    @GetMapping("getFileContent")
    public ApiResult getFileContent(@RequestParam String path){
         return service.getFileContent(path);
    }

    @PostMapping("setFileContent")
    public boolean setFileContent(@RequestBody TemplateContentDto templateContentDto){
        return service.setFileContent(templateContentDto.getPath(),templateContentDto.getContent());
    }

    @DeleteMapping("delFile")
    public boolean delFile(@NotBlank(message = "文件目录不能为空!") @RequestParam String path){
        return service.delFile(path);
    }

    /**
     * 导入模板
     * @param files
     * @return
     */
    @PostMapping("uploadFile")
    public ApiResult uploadFile(@RequestParam("files[]") List<MultipartFile> files){
        return service.uploadFile(files);
    }

    /**
     * 上传文件
     * @param files
     * @return
     */
    @PostMapping("importFile")
    public ApiResult importFile(@RequestParam("path")String path,@RequestParam("files[]") List<MultipartFile> files) throws IOException {
        return service.importFile(path,files);
    }

    /**
     * 解压文件
     * @return
     */
    @PostMapping("unZipFile")
    public ApiResult unZipFile(@RequestParam("path") String path) throws IOException {
        return service.unZipFile(path);
    }

    /**
     * 导出模板
     * @param tempId
     * @return
     */
    @PostMapping("exportit")
    public void exportit(@RequestParam("tempId") String tempId,HttpServletResponse response){
         service.exportit(tempId,response);
    }


    /**
     * 下载模板文件
     * @param path
     * @return
     */
    @PostMapping("downFiles")
    public void downFiles(@RequestParam("path") String path,HttpServletResponse response){
        service.downFiles(path,response);
    }



    @GetMapping(value = "/getDirects")
    public ApiResult getDirects(){
        return  service.getDirects();
    }

}
