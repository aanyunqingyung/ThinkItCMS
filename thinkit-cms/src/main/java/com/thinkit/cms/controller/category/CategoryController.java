package com.thinkit.cms.controller.category;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.core.annotation.NotDecorate;
import com.thinkit.core.annotation.SiteMark;
import com.thinkit.core.base.BaseController;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.KeyValueModel;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.model.Tree;
import com.thinkit.utils.properties.PermitAllUrlProperties;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分类 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-07
 */
@Validated
@RestController
@RequestMapping("category")
public class CategoryController extends BaseController<CategoryService> {

    @Autowired
    PermitAllUrlProperties permitAllUrlProperties;

    @NotDecorate
    @GetMapping(value="getDetail")
    public Map<String, Object> getDetail(@NotBlank @RequestParam String id){
        return service.getDetail(id);
    }


    @PostMapping(value="save")
    public ApiResult save(@Validated @RequestBody CategoryDto v){
       return service.save(v);
    }

    @PutMapping("update")
    public void update(@RequestBody CategoryDto v){
        service.update(v);
    }

    @DeleteMapping("deleteByPk")
    public boolean deleteByPk(@NotBlank @RequestParam String id) {
         return service.deleteById(id);
    }

    @PostMapping("list")
    public  List<CategoryDto> list(@RequestBody CategoryDto v){
        return service.listDto(v);
    }

    @SiteMark
    @PostMapping("page")
    public PageDto<CategoryDto> listPage(@RequestBody PageDto<CategoryDto> pageDto){
        if(Checker.BeBlank(pageDto.getDto().getParentId())){
            pageDto.getDto().setParentId("0");
        }
        return service.listPage(pageDto);
    }

    // 栏目管理用
    @GetMapping("/treeCategory")
    public Tree<CategoryDto> treeCategory() {
        Tree<CategoryDto> treeCategorys = service.treeCategory();
        return treeCategorys;
    }


    @GetMapping("loadTemplate")
    public Map<String,Object> loadTemplateTree(){
        return service.loadTemplate();
    }

    @GetMapping("loadPathRule")
    public List<KeyValueModel> loadPathRule(){
        return service.loadPathRule();
    }



    @PutMapping(value="genCategory")
    public void genCategory(@NotBlank @RequestParam String id){
         service.genCategory(id);
    }



}
