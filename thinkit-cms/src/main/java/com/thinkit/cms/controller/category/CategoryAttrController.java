package com.thinkit.cms.controller.category;

import com.thinkit.cms.api.category.CategoryAttrService;
import com.thinkit.cms.dto.category.CategoryAttrDto;
import com.thinkit.core.base.BaseController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 分类扩展 前端控制器
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
@Validated
@RestController
@RequestMapping("categoryAttr")
public class CategoryAttrController extends BaseController<CategoryAttrService> {

    @PostMapping("updateSeo")
    public void update(@Validated @RequestBody CategoryAttrDto v){
        service.updateSeo(v);
    }

    @GetMapping("getSeo")
    public CategoryAttrDto getSeo(@RequestParam String categoryId){
        return service.getSeo(categoryId);
    }


}
