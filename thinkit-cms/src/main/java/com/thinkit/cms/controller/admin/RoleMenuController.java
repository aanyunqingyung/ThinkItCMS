package com.thinkit.cms.controller.admin;
import com.thinkit.cms.api.admin.RoleMenuService;
import com.thinkit.cms.dto.admin.MenuDto;
import com.thinkit.cms.dto.admin.RoleMenuDto;
import com.thinkit.core.base.BaseController;
import com.thinkit.log.annotation.Logs;
import com.thinkit.log.enums.LogModule;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.Tree;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色与菜单对应关系 前端控制器
 * </p>
 *
 * @author dl
 * @since 2018-03-23
 */
@RestController
@RequestMapping("/role/roleMenu")
public class RoleMenuController extends BaseController<RoleMenuService> {
    @Autowired
    RoleMenuService roleMenuService;

    @RequestMapping("/selectTreeMenuByRole")
    public Tree<MenuDto> selectTreeMenuByRole(@RequestParam String roleId) {
        Tree<MenuDto> menus = roleMenuService.selectTreeMenuByUser(roleId, getUserId());
        return menus;
    }

    @Logs(module = LogModule.ROLE,operation = "给角色分配菜单")
    @RequestMapping("/assignMenu")
    public ApiResult assignMenu(@RequestBody RoleMenuDto roleMenuDto) {
        if (Checker.BeEmpty(roleMenuDto.getOnCheckKeys()) || Checker.BeBlank(roleMenuDto.getRoleId())) {
            return ApiResult.result("参数异常!", -1);
        }
        boolean f = roleMenuService.assignMenu(roleMenuDto);
        if (f) {
            return ApiResult.result();
        } else
            return ApiResult.result("操作失败", -1);
    }
}
