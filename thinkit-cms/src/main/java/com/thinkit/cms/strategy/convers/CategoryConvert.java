package com.thinkit.cms.strategy.convers;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.core.constant.Constants;
import com.thinkit.utils.enums.ActionEnum;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CategoryConvert {

    public String convertParam(Object params, ActionEnum actionEnum){
        if(Checker.BeNotNull(params)){
            if(ActionEnum.CREATE_CONTENT.equals(actionEnum) ||
               ActionEnum.UPDATE_CONTENT.equals(actionEnum) ||
               ActionEnum.TOP_CONTENT.equals(actionEnum)){ // 创建,更新,置顶
               ContentDto content = (ContentDto) params;
               return  content.getCategoryId();
            }else if(ActionEnum.GENERATE_CATEGORY.equals(actionEnum) ||
                    ActionEnum.DELETE_CONTENT.equals(actionEnum)){ // 静态化 or 删除
                Map map = (Map) params;
                return map.get(Constants.categoryId).toString();
            }else if(ActionEnum.PUBLISH_CONTENT.equals(actionEnum) || ActionEnum.BATCH_MOVE.equals(actionEnum)){ // 发布 or 移动
                return params.toString();
            }else if(ActionEnum.UPDATE_CATEGORY.equals(actionEnum)||ActionEnum.CREATE_CATEGORY.equals(actionEnum)){ //栏目新增修改
                CategoryDto category = (CategoryDto) params;
                return  category.getId();
            }
        }
        return null;
    }

}
