package com.thinkit.cms.mapper.admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.admin.MemberDto;
import com.thinkit.cms.entity.admin.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

    MemberDto loadMemUserByUsername(@Param("account") String account);
}
