package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.admin.OrgDto;
import com.thinkit.cms.entity.admin.Org;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrgMapper extends BaseMapper<Org> {
	
	void deleteByOrgCode(@Param("code") String code);

    List<OrgDto> getOrgsByUserId(@Param("orgCode") String orgCode);

	List<String> listUserIdsByOrgAndRole(@Param("orgIds") List<String> orgIds, @Param("roleCode") String roleCode);

    Integer ckCode(@Param("code") String code, @Param("id") String id);
}
