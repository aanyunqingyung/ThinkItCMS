package com.thinkit.cms.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.entity.admin.Analysis;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 流量统计配置 Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2020-11-25
 */
@Mapper
public interface AnalysisMapper extends BaseMapper<Analysis> {

}
