package com.thinkit.cms.mapper.site;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.entity.site.Site;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 站点管理表 Mapper 接口
 * </p>
 *
 * @author LG
 * @since 2020-07-22
 */
@Mapper
public interface SiteMapper extends BaseMapper<Site> {

    Integer ckHasDomain(@Param("id") String id,@Param("domain") String domain);

    void setDefault(@Param("siteId") String siteId);

    String getDefault();

    List<SiteDto> getAllSite(@Param("dto") SiteDto v);

    String getDestPath(@Param("containCode") boolean containCode,@Param("siteId")  String siteId);

    Map<String,Object> findHomePageFile(@Param("siteId") String siteId);

    Map<String, Object> direFoeSiteInfo(@Param("siteId") String siteId);

    String getDomain(@Param("schema") String schema,@Param("siteId") String siteId);

    Map<String,Object> getDomainCode(@Param("urlPrefix") String urlPrefix,@Param("siteId") String siteId);

    List<SiteDto> getMySites(@Param("userId") String userId,@Param("orgId") String orgId);

    IPage<SiteDto> listPage(IPage<ContentDto> pages, @Param("dto") SiteDto dto,@Param("userId") String userId,@Param("orgId") String orgId);

    String domainCode(@Param("siteId") String siteId);
}
