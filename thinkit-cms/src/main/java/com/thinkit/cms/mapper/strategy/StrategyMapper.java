package com.thinkit.cms.mapper.strategy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.dto.strategy.StrategyDto;
import com.thinkit.cms.entity.strategy.Strategy;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lg
 * @since 2021-05-12
 */
@Mapper
public interface StrategyMapper extends BaseMapper<Strategy> {

    List<StrategyDto> queryStrategy(@Param("categoryId") String categoryId,
                                    @Param("actionCode") String actionCode,
                                    @Param("isSatrt") Integer isSatrt);
}
