package com.thinkit.cms.service.upload.impl;

import com.thinkit.cms.api.upload.UploadService;
import com.thinkit.cms.dto.upload.Chunk;
import com.thinkit.cms.service.upload.client.BaseUpload;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Service
public class UploadServiceImpl extends BaseUpload implements UploadService {

    @Override
    public ApiResult uploadFile(MultipartFile file) {
        ApiResult result = uploadClient.readyUpload(file);
        if (result.ckSuccess()) {
            result = uploadClient.uploadFile(file);
            if (result.ckSuccess()) {
                result = uploadClient.uploadSuccess(result, file);
            } else {
                uploadClient.uploadError(result);
            }
        } else {
            uploadClient.uploadError(result);
        }
        return result;
    }

    @Override
    public ApiResult uploadFile(String remoteUrl) {
        return uploadClient.uploadFile(remoteUrl);
    }

    @Override
    public ApiResult keepUploadFile(Chunk chunk) {
        MultipartFile file=chunk.getFile();
        String finshFlag="finshed";
        ApiResult result = uploadClient.readyUpload(file);
        if (result.ckSuccess()) {
            result = uploadClient.keepUploadFile(chunk);
            Map<String,Object> params=(Map) result.get("res");
            if(Checker.BeNotEmpty(params)){
                if (params.containsKey(finshFlag) && (Boolean) params.get(finshFlag)) {
                    result = uploadClient.uploadSuccess(result, file);
                }
            }
        } else {
            uploadClient.uploadError(result);
        }
        return result;
    }


    @Override
    public ApiResult deleteFile(Map<String, String> params) {
        return uploadClient.deleteFile(params);
    }

    @Override
    public ApiResult checkFileIsExist(Chunk chunk) {
        Map<String,Object> resultMap=checkFileIsExistByMd5(chunk.getIdentifier());
        return ApiResult.result(resultMap);
    }

}
