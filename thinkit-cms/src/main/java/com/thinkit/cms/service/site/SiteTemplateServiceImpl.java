package com.thinkit.cms.service.site;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.site.SiteTemplateService;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.dto.site.SiteTemplateDto;
import com.thinkit.cms.dto.template.TemplateDto;
import com.thinkit.cms.entity.site.SiteTemplate;
import com.thinkit.cms.mapper.template.SiteTemplateMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.core.constant.Constants;
import com.thinkit.core.handler.CustomException;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.nio.charset.Charset;

/**
 * <p>
 * 站点模型表 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Transactional
@Service
public class SiteTemplateServiceImpl extends BaseServiceImpl<SiteTemplateDto, SiteTemplate, SiteTemplateMapper> implements SiteTemplateService {

    @Autowired
    TemplateService templateService;

    @Autowired
    SiteService siteService;

    @Autowired
    ThinkItProperties thinkItProperties;

    @Transactional
    @Override
    public void inunstall(SiteTemplateDto v) {
         SiteTemplateDto siteTemplateDto= findInstall(v);
         ckTemplate(v);
         if(v.getStartUsing()==1){ // 安装
             // 安装之前停用所有模板
             unstallAll(v.getSiteId());
             if(ckInstall(siteTemplateDto)){
                 siteTemplateDto.setStartUsing(1);
                 super.updateByPk(siteTemplateDto);
             }else{
                 super.insert(v);
             }
             installSource(v);
         }else if(v.getStartUsing()==0){// 禁用
             if(!ckInstall(siteTemplateDto)){
                  throw new CustomException(ApiResult.result(5023));
             }
             siteTemplateDto.setStartUsing(0);
             super.updateByPk(siteTemplateDto);
         }
    }

    /**
     * 安装资源目录
     * @param v
     */
    private void installSource(SiteTemplateDto v){
       String tempPath = thinkItProperties.getTemplate()+v.getTemplateFolderPath();
       if(FileUtil.exist(tempPath)){
           File  tempFolder = new File(tempPath);
           for(File file:tempFolder.listFiles()){
                if(file.getName().contains("config.json")){
                    String content = FileUtil.readString(file, Charset.forName("utf-8"));
                    if(Checker.BeNotBlank(content)){
                        TemplateDto templateDto = JSONUtil.toBean(content,TemplateDto.class);
                        String sourcePath = tempPath+Constants.SEPARATOR+templateDto.getSourceFolder();
                        if(FileUtil.exist(sourcePath)){
                            String domain = siteService.getDomain(null,v.getSiteId());
                            String dest = thinkItProperties.getSitePath()+Constants.SEPARATOR+domain+Constants.SEPARATOR+
                            templateDto.getTemplateCode();
                            FileUtil.copy(new File(sourcePath),new File(dest),true);
                        }
                    }
                }
           }
       }
    }

    @Override
    public String findTemplatePath(String siteId) {
        return baseMapper.findTemplatePath(thinkItProperties.getTemplate(),siteId);
    }

    @Override
    public SiteTemplateDto findTemplate(String siteId) {
        return baseMapper.findTemplate(thinkItProperties.getTemplate(),siteId);
    }

    @Override
    public String findTemplateId(String siteId) {
        return baseMapper.findTemplateId(siteId);
    }

    @Override
    public void updateTempLocation(String templateId, String loaction) {
        baseMapper.updateTempLocation(templateId,loaction);
    }

    private void unstallAll(String siteId){
        baseMapper.unstallAll(siteId);
    }

    private void ckTemplate(SiteTemplateDto v){
        TemplateDto templateDto=templateService.getByPk(v.getTemplateId());
        if(Checker.BeNull(templateDto)){
            throw new CustomException(ApiResult.result(5024));
        }
        v.setTemplateFolderPath(templateDto.getTemplateFolderPath());
    }

    private boolean ckInstall(SiteTemplateDto v){
       return Checker.BeNotNull(v);
    }

    private SiteTemplateDto findInstall(SiteTemplateDto v){
        return baseMapper.ckInstall(v.getSiteId(),v.getTemplateId());
    }
}
