package com.thinkit.cms.service.channel;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.core.constant.Channel;
import com.thinkit.processor.channel.BaseChannelAdaptService;
import com.thinkit.processor.channel.ChannelThreadLocal;
import com.thinkit.utils.enums.ChannelEnum;
import com.thinkit.utils.properties.ThinkItProperties;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class ContentChannelService extends BaseChannelAdaptService implements Cloneable  {

    @Autowired
    ContentService contentService;

    @Autowired
    ThinkItProperties thinkItProperties;

    Map<String,Object> params = new HashMap<>();

    public void notifyIt(Map<String,Object> params){
        params.put(Channel.SITE_ID, ChannelThreadLocal.get(Channel.SITE_ID));
        this.params = params;
        execuate();
    }

    public void notifyIt(Map<String,Object> params,Boolean notify){
        params.put(Channel.SITE_ID, ChannelThreadLocal.get(Channel.SITE_ID));
        this.params = params;
        if(notify){
            execuate(true,true);
        }else {
            execuate();
        }
    }

    @Override
    protected String loadTempPath() {
        return this.tempPath;
    }

    @Override
    protected String loadDestPath() {
        return this.destPath;
    }

    @Override
    protected Map<String, Object> loadTempParams(Map<String,Object> variable) {
        Map<String,Object> params = contentService.loadTempParams(this.params, Arrays.asList("0","1"));
        setPath(params);
        return params;
    }

    public void setPath(Map<String,Object> params){
        if(Checker.BeNotNull(params) && !params.isEmpty()){
            boolean hasTemp=params.containsKey(Channel.TEMP_PATH) && Checker.BeNotNull(params.get(Channel.TEMP_PATH));
            boolean hasDest=params.containsKey(Channel.DEST_PATH) && Checker.BeNotNull(params.get(Channel.DEST_PATH));
            boolean hasPathRule=params.containsKey(Channel.PATH_RULE) && Checker.BeNotNull(params.get(Channel.PATH_RULE));
            if(hasTemp){
                this.tempPath =params.get(Channel.TEMP_PATH).toString();
            }
            if(hasDest){
                String templatePath =thinkItProperties.getSitePath();
                String destPath = params.get(Channel.DEST_PATH).toString();
                String url = params.get(Channel.URL).toString();
                if(hasPathRule){
                    this.destPath = templatePath+destPath+url;
                    params.put(Channel.DEST_PATH,this.destPath.replace("\\","/"));
                }
            }
        }
    }


    @Override
    public ChannelEnum getName() {
        return ChannelEnum.CONTENT;
    }
}
