package com.thinkit.cms.service.job.impl;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.job.JobService;
import com.thinkit.cms.dto.content.PublishDto;
import com.thinkit.cms.service.job.bean.ContentPublishJob;
import com.thinkit.cms.service.job.handler.PublishHandler;
import com.thinkit.cms.strategy.filter.ActuatorFilter;
import com.thinkit.core.annotation.Actuator;
import com.thinkit.core.base.BaseContextKit;
import com.thinkit.core.constant.Channel;
import com.thinkit.core.constant.Constants;
import com.thinkit.nosql.base.BaseRedisService;
import com.thinkit.processor.job.JobActionNotify;
import com.thinkit.processor.job.JobExecute;
import com.thinkit.utils.enums.ActuatorEnum;
import com.thinkit.utils.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    JobExecute jobExecute;

    @Autowired
    ContentService contentService;

    @Autowired
    BaseRedisService baseRedisService;

    @Override
    public void jobPublish(PublishDto publishDto) {
         if(Checker.BeNotEmpty(publishDto.getIds())){
             List<String> ids = publishDto.getIds();
             Map jobBean = new HashMap();
             jobBean.put("bean",publishDto);
             jobExecute.execute(JobActionNotify.CONTENT_PUBLISH,publishDto.getDate(), ContentPublishJob.class,jobBean,new PublishHandler());
             contentService.jobPublish(ids,publishDto.getDate());
         }
    }

    public void addJobs(List<ActuatorFilter> jobFilters) {
         if(Checker.BeNotEmpty(jobFilters)){
             for(ActuatorFilter filter:jobFilters){
                 addJobToNosql(filter);
             }
         }
    }


    private void addJobToNosql(ActuatorFilter filter){
        if(Checker.BeNotNull(filter)){
            Actuator actuator = filter.getClass().getAnnotation(Actuator.class);
            if(Checker.BeNotNull(actuator)){
                Object params = filter.getParam();
                String key =setNoSql(actuator.code(),filter.triggerTime(),params);
                if(!baseRedisService.hasKey(key)){
                    Map<String,Object> param = new HashMap<>();
                    param.put("params",params);
                    param.put(Channel.SITE_ID,BaseContextKit.getSiteId());
                    param.put(Channel.USER_ID,BaseContextKit.getUserId());
                    jobExecute.execute(actuator.name(),actuator.code(),filter.triggerTime(),filter.getClass(),param);
                    baseRedisService.set(key,params);
                }
            }
        }
    }

    private String setNoSql( String code,String triggerTime,Object params ){
        String flag =":", finalKey = "";
        if(Checker.BeNotBlank(code)){
            String baseKey = Constants.JOB_NAME+BaseContextKit.getSiteId()+flag;
            ActuatorEnum actuatorEnum = ActuatorEnum.getActuatorEnum(code);
            if(actuatorEnum.equals(ActuatorEnum.INDEX_ACTUATOR)){
                finalKey = baseKey+code+flag+triggerTime;
            }else if(actuatorEnum.equals(ActuatorEnum.INDEX_ACTUATOR)){

            }
        }
        return finalKey;
    }
}
