package com.thinkit.cms.service.tag;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.tag.TagService;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.cms.entity.tag.Tag;
import com.thinkit.cms.mapper.tag.TagMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.nosql.annotation.CacheClear;
import com.thinkit.utils.model.PageDto;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标签 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
@Transactional
@Service
public class TagServiceImpl extends BaseServiceImpl<TagDto, Tag, TagMapper> implements TagService {


    @Override
    public List<TagDto> listTags(List<String> tags, String siteId) {
        if(Checker.BeNotEmpty(tags)){
            List<TagDto> tagDtos=baseMapper.listTags(tags,siteId);
            return Checker.BeNotEmpty(tagDtos) ? tagDtos :Lists.newArrayList();
        }
        return Lists.newArrayList();
    }

    @Override
    public List<String> listTags(List<String> tagIds) {
        return baseMapper.listTagByIds(tagIds);
    }

    @Override
    public PageDto<TagDto> listPage(PageDto<TagDto> pageDto) {
        IPage<TagDto> pages = new Page<>(pageDto.getPageNo(), pageDto.getPageSize());
        IPage<TagDto> result = baseMapper.listPage(pages, pageDto.getDto());
        PageDto<TagDto> resultSearch = new PageDto(result.getTotal(), result.getPages(), result.getCurrent(), Checker.BeNotEmpty(result.getRecords()) ? result.getRecords() : Lists.newArrayList());
        return resultSearch;
    }


    @Override
    public List<String> saveTags(List<String> tags) {
        List<String> tagIds = new ArrayList<>(16);
        List<TagDto> cmsTagsDtos = new ArrayList<>(16);
        Map<String,String> tagRes=selectTagsByName(tags);
        if(Checker.BeNotEmpty(tags)){
            for(String tag :tags){
                String dbTagId = tagRes.get(tag);
                if(Checker.BeNotBlank(dbTagId)){
                    tagIds.add(dbTagId);
                    continue;
                }
                String id =id();
                tagIds.add(id);
                TagDto cmsTags =new TagDto();
                cmsTags.setName(tag.toUpperCase().replaceAll("\\s*", "")).
                setId(id).setSiteId(getSiteId());
                cmsTagsDtos.add(cmsTags);
            }
            insertBatch(cmsTagsDtos);
        }
        return tagIds;
    }

    private  Map<String,String> selectTagsByName(List<String> tags){
        Map<String,String> tagRes=new HashMap<>();
        if(Checker.BeNotEmpty(tags)){
            List<String> formaterTags=new ArrayList<>(16);
            tags.forEach(tag->{
                formaterTags.add(tag.toUpperCase().replaceAll("\\s*", ""));
            });
            List<TagDto> tagsDtos=listByField("name",formaterTags);
            if(Checker.BeNotEmpty(tagsDtos)){
                tagsDtos.forEach(tagsDto->{
                    tagRes.put(tagsDto.getName().toUpperCase().replaceAll("\\s*", ""),tagsDto.getId());
                });
            }
        }
        return tagRes;
    }

    // 判断数据库标签是否存在 存在则不插入
    private String ckExistTag(String tag){
        String tagId = "";
        TagDto cmsTagsDto =getByField("name",tag.toUpperCase().replaceAll("\\s*", ""));
        if(Checker.BeNotNull(cmsTagsDto)){
            tagId = cmsTagsDto.getId();
        }
        return tagId;
    }
}
