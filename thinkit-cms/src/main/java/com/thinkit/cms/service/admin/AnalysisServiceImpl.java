package com.thinkit.cms.service.admin;

import com.thinkit.cms.api.admin.AnalysisService;
import com.thinkit.cms.dto.admin.AnalysisDto;
import com.thinkit.cms.entity.admin.Analysis;
import com.thinkit.cms.mapper.admin.AnalysisMapper;
import com.thinkit.core.base.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 流量统计配置 服务实现类
 * </p>
 *
 * @author lg
 * @since 2020-11-25
 */
@Transactional
@Service
public class AnalysisServiceImpl extends BaseServiceImpl<AnalysisDto, Analysis, AnalysisMapper> implements AnalysisService {



}
