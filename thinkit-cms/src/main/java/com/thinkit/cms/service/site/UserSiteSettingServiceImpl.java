package com.thinkit.cms.service.site;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.site.UserSiteSettingService;
import com.thinkit.cms.dto.site.UserSiteSettingDto;
import com.thinkit.cms.entity.site.UserSiteSetting;
import com.thinkit.cms.mapper.site.UserSiteSettingMapper;
import com.thinkit.core.base.BaseServiceImpl;
import com.thinkit.utils.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户站点设置表 服务实现类
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
@Transactional
@Service
public class UserSiteSettingServiceImpl extends BaseServiceImpl<UserSiteSettingDto, UserSiteSetting, UserSiteSettingMapper> implements UserSiteSettingService {


    @Override
    public UserSiteSettingDto getBySiteId(String siteId) {
        UserSiteSettingDto userSiteSettingDto =baseMapper.getBySiteId(siteId,getUserId());
        return userSiteSettingDto;
    }

    @Override
    public List<UserSiteSettingDto> getByUserId(String userId) {
        List<UserSiteSettingDto> userSiteSettingDto =baseMapper.getByUserId(userId);
        return Checker.BeNotEmpty(userSiteSettingDto) ? userSiteSettingDto: Lists.newArrayList();
    }

    @Override
    public void setDefaultSetting(String siteId) {
        UserSiteSettingDto userSiteSettingDto= getBySiteId(siteId);
        baseMapper.updateDefault(getUserId());
        if(Checker.BeNotNull(userSiteSettingDto)){
            userSiteSettingDto.setIsDefault(true);
            super.updateByPk(userSiteSettingDto);
        }else{ // 创建
            UserSiteSettingDto siteSettingDto = new UserSiteSettingDto();
            siteSettingDto.setIsDefault(true).setOrgId(getOrgId()).setSiteId(siteId).setUserId(getUserId());
            super.insert(siteSettingDto);
        }
    }

    @Override
    public void saveSetting(String orgId, String userId, String siteId) {
        UserSiteSettingDto siteSettingDto = new UserSiteSettingDto();
        siteSettingDto.setIsDefault(true).setOrgId(orgId).setSiteId(siteId).setUserId(userId);
        super.insert(siteSettingDto);
    }
}
