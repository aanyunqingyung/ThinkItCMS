package com.thinkit.cms.entity.category;
import java.io.Serializable;
import com.thinkit.utils.model.BaseModel;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * <p>
 * 分类选择模型时关系表 该模型值 tk_model 表 不是 分类模型表
 * </p>
 *
 * @author LG
 * @since 2020-08-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_category_model_relation")
public class CategoryModelRelation extends BaseModel {



        @TableField("site_id")
        private String siteId;



        @TableField("model_id")
        private String modelId;



        @TableField("category_id")
        private String categoryId;


        @TableField("template_id")
        private String templateId;


        @TableField("template_path")
        private String templatePath;



}
