package com.thinkit.cms.entity.content;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * <p>
 * 内容扩展
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_content_attr")
public class ContentAttr extends BaseModel {

    /**
     * 主键
     */

        @TableField("content_id")
        private String contentId;


    /**
     * 内容来源
     */

        @TableField("origin")
        private String origin;


    /**
     * 来源地址
     */

        @TableField("origin_url")
        private String originUrl;


    /**
     * 数据JSON
     */

        @TableField("data")
        private String data;


    /**
     * 内容
     */

        @TableField("text")
        private String text;


    /**
     * 字数
     */

        @TableField("total_word_count")
        private Integer totalWordCount;


}
