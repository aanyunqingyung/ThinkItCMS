package com.thinkit.cms.entity.site;
import java.io.Serializable;
import com.thinkit.utils.model.BaseModel;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * <p>
 * 用户站点设置表
 * </p>
 *
 * @author LG
 * @since 2020-09-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("tk_user_site_setting")
public class UserSiteSetting extends BaseModel {

private static final long serialVersionUID = 1L;


        @TableField("user_id")
        private String userId;



        @TableField("site_id")
        private String siteId;



        @TableField("org_id")
        private String orgId;



        @TableField("is_default")
        private Boolean isDefault;


}
