package com.thinkit.cms.entity.admin;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author LG
 * @since 2020-05-05
 */
@Data
@Accessors(chain = true)
@TableName("sys_api_source_client")
public class ApiClient extends BaseModel {


    /**
     * 接口资源ID
     */

        @TableField("api_source_id")
        private String apiSourceId;



        @TableField("client_id")
        private String clientId;


    /**
     * 微服务实例id
     */

        @TableField("service_id")
        private String serviceId;


    /**
     * 0：半选中 1：全选中
     */

        @TableField("half_checked")
        private Integer halfChecked;

}
