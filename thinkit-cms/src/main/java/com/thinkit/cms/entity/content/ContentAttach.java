package com.thinkit.cms.entity.content;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * <p>
 * 内容附件
 * </p>
 *
 * @author lg
 * @since 2020-08-19
 */
@Data
@Accessors(chain = true)
@TableName("tk_content_attach")
public class ContentAttach extends BaseModel {


    /**
     * 内容
     */

        @TableField("content_id")
        private String contentId;


    /**
     * 关联资源表
     */

        @TableField("file_uid")
        private String fileUid;



        @TableField("url")
        private String url;



        @TableField("name")
        private String name;


    /**
     * 下载数
     */

        @TableField("downs")
        private Integer downs;


    /**
     * 排序
     */

        @TableField("sort")
        private Integer sort;


    /**
     * json 字段
     */

        @TableField("data")
        private String data;


    /**
     * 创建用户id
     */



    /**
     * 修改人id
     */



    /**
     * 创建时间
     */



    /**
     * 修改时间
     */






}
