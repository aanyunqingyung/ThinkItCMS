package com.thinkit.cms.entity.site;
import java.io.Serializable;
import com.thinkit.utils.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * <p>
 * 站点模型表
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Data
@Accessors(chain = true)
@TableName("tk_site_template")
public class SiteTemplate extends BaseModel {

       private static final long serialVersionUID = 1L;

    /**
     * 站点id
     */

        @TableField("site_id")
        private String siteId;


    /**
     * 模型id
     */

        @TableField("template_id")
        private String templateId;


    /**
     * 模板路径
     */

        @TableField("template_folder_path")
        private String templateFolderPath;


    /**
     * 0:未启用 1：已启用
     */

        @TableField("start_using")
        private Integer startUsing;

}
