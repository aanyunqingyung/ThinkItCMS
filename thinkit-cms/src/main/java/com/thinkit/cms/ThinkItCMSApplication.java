package com.thinkit.cms;
import io.swagger.annotations.ApiOperation;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@ComponentScan(basePackages={"com.thinkit.**"})
@MapperScan(basePackages ={"com.thinkit.*.mapper.**"})
@EnableSwagger2WebMvc
public class ThinkItCMSApplication {
    public static void main(String[] args) {
        SpringApplicationBuilder springApplicationBuilder =new SpringApplicationBuilder(ThinkItCMSApplication.class);
        springApplicationBuilder.headless(false).run(args);
        System.out.println("\n" +
                "__/\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\__/\\\\\\______________________________________________________/\\\\\\\\\\\\\\\\\\\\\\________________________________/\\\\\\\\\\\\\\\\\\__/\\\\\\\\____________/\\\\\\\\_____/\\\\\\\\\\\\\\\\\\\\\\___        \n" +
                " _\\///////\\\\\\/////__\\/\\\\\\______________________________/\\\\\\___________________\\/////\\\\\\///______________________________/\\\\\\////////__\\/\\\\\\\\\\\\________/\\\\\\\\\\\\___/\\\\\\/////////\\\\\\_       \n" +
                "  _______\\/\\\\\\_______\\/\\\\\\__________/\\\\\\_______________\\/\\\\\\_______________________\\/\\\\\\_________/\\\\\\__________________/\\\\\\/___________\\/\\\\\\//\\\\\\____/\\\\\\//\\\\\\__\\//\\\\\\______\\///__      \n" +
                "   _______\\/\\\\\\_______\\/\\\\\\_________\\///___/\\\\/\\\\\\\\\\\\___\\/\\\\\\\\\\\\\\\\__________________\\/\\\\\\______/\\\\\\\\\\\\\\\\\\\\\\____________/\\\\\\_____________\\/\\\\\\\\///\\\\\\/\\\\\\/_\\/\\\\\\___\\////\\\\\\_________     \n" +
                "    _______\\/\\\\\\_______\\/\\\\\\\\\\\\\\\\\\\\___/\\\\\\_\\/\\\\\\////\\\\\\__\\/\\\\\\////\\\\\\________________\\/\\\\\\_____\\////\\\\\\////____________\\/\\\\\\_____________\\/\\\\\\__\\///\\\\\\/___\\/\\\\\\______\\////\\\\\\______    \n" +
                "     _______\\/\\\\\\_______\\/\\\\\\/////\\\\\\_\\/\\\\\\_\\/\\\\\\__\\//\\\\\\_\\/\\\\\\\\\\\\\\\\/_________________\\/\\\\\\________\\/\\\\\\________________\\//\\\\\\____________\\/\\\\\\____\\///_____\\/\\\\\\_________\\////\\\\\\___   \n" +
                "      _______\\/\\\\\\_______\\/\\\\\\___\\/\\\\\\_\\/\\\\\\_\\/\\\\\\___\\/\\\\\\_\\/\\\\\\///\\\\\\_________________\\/\\\\\\________\\/\\\\\\_/\\\\_____________\\///\\\\\\__________\\/\\\\\\_____________\\/\\\\\\__/\\\\\\______\\//\\\\\\__  \n" +
                "       _______\\/\\\\\\_______\\/\\\\\\___\\/\\\\\\_\\/\\\\\\_\\/\\\\\\___\\/\\\\\\_\\/\\\\\\_\\///\\\\\\____________/\\\\\\\\\\\\\\\\\\\\\\____\\//\\\\\\\\\\________________\\////\\\\\\\\\\\\\\\\\\_\\/\\\\\\_____________\\/\\\\\\_\\///\\\\\\\\\\\\\\\\\\\\\\/___ \n" +
                "        _______\\///________\\///____\\///__\\///__\\///____\\///__\\///____\\///____________\\///////////______\\/////____________________\\/////////__\\///______________\\///____\\///////////_____\n");
    }



    @Bean
    public Docket createRestApi() {
        //添加header参数
        ParameterBuilder ticketPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        ticketPar.name("Authorization").description("JWT")
        .modelRef(new ModelRef("string")).parameterType("header")
        .required(false).build(); //header中的ticket参数非必填，传空也可以
        pars.add(ticketPar.build());    //根据每个方法名也知道当前方法在设置什么参数

        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
            .apis(RequestHandlerSelectors.basePackage("com.thinkit.cms.controller"))
            .paths(PathSelectors.any())
            .build().groupName("2.X版本").apiInfo(apiInfo()).globalOperationParameters(pars);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
        .title("ThinkItCMS-项目服务接口")
        .description("Api接口文档对接")
        .version("2.0").contact(new Contact("LG","http://localhost:8115/doc.htm","826319429@qq.com"))
        .build();
    }

}
