package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 分类扩展
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryAttrDto extends BaseDto {

        /**
        * 分类ID
        */
        @NotBlank(message = "分类不能为空")
        private String categoryId;


        /**
        * 标题(分类标题用于 SEO 关键字优化)
        */
        @NotBlank(message = "标题不能为空")
        private String title;


        /**
        * 关键词用于 SEO 关键字优化
        */
        private String keywords;


        /**
        * 描述用于 SEO 关键字优化
        */
        private String description;


        /**
        * 数据JSON
        */
        private String data;











}
