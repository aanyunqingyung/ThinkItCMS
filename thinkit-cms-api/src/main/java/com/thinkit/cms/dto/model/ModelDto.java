package com.thinkit.cms.dto.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author LG
 * @since 2019-10-23
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelDto extends BaseDto {

    private String name;
    /**
     * 模型名称
     */

    private Boolean check = false;


    private String templatePath ;

    private String md5Key;


    private String siteId;


    /**
     * 是否有用图片
     */

    private Boolean hasImages;


    /**
     * 是否拥有文件
     */

    private Boolean hasFiles;


    private Boolean isUrl;


    private Boolean supportWehcat;

    private Boolean supportTops;


    /**
     * 全部选中字段
     */
    private String allFieldList;


    /**
     * 选中字段
     */

    private String defaultFieldList;


    /**
     * 扩展字段
     */

    private String extendFieldList;


    /**
     * 必填字段
     */

    private String requiredFieldList;


    /**
     * 字段对应中文名称
     */

    private String fieldTextMap;


}
