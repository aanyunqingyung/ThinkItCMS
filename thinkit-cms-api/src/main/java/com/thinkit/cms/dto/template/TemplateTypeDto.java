package com.thinkit.cms.dto.template;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 模板分类
 * </p>
 *
 * @author lg
 * @since 2020-07-28
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TemplateTypeDto extends BaseDto {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "分类名称不能为空")
    private String name;

    private String parentName;

    @NotBlank(message = "分类编码不能为空")
    private String code;


    private String parentId;


    /**
     * 分类状态 0：显示 1：隐藏
     */
    private Integer status;


}
