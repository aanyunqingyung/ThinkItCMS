package com.thinkit.cms.dto.upload;

import lombok.Data;

@Data
public class UploadDto {


    private String uid;

    private String name;

    private String url;

    private String thumbUrl;
}
