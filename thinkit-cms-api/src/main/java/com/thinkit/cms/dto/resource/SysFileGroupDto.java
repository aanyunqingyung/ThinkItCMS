package com.thinkit.cms.dto.resource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 
 * </p>
 *
 * @author sysFileGroup
 * @since 2021-07-03
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysFileGroupDto extends BaseDto {


        /**
        * 分组名称
        */
        @NotBlank(message = "分组名称不能为空")
        private String name;


        /**
        * 分组编码
        */
        private String code;


        /**
        * 是否是系统分组，系统分组禁止删除
        */
        private Integer isSysGroup;


        /**
        * 排序
        */
        private Integer sort;


        private Integer fileCount;

}
