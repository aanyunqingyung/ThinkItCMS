package com.thinkit.cms.dto.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lg
 * @since 2020-11-03
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberDto extends BaseDto {


        /**
        * 真实姓名
        */
        private String realName;


        /**
        * 会员昵称
        */
        private String nickName;


        /**
        * 头像
        */
        private String headImg;


        /**
        * 账号
        */
        private String account;


        /**
        * 密码
        */
        private String password;


        /**
        * 手机号
        */
        private String tel;


        /**
        * 邮箱
        */
        private String email;


        /**
        * 性别
        */
        private Integer sex;


        /**
        * 注册时间
        */
        private LocalDateTime registerDate;


        /**
        * 是否锁定 0：未锁定 1：已锁定
        */
        private Integer isLock;


        /**
        * 0:未删除 1：已删除
        */
        private Integer isDel;

}
