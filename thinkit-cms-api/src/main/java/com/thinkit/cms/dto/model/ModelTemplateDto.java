package com.thinkit.cms.dto.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;
/**
 * <p>
 * 模型模板针对不同的站点不同的模型下有不同的 模板页面
 * </p>
 *
 * @author lg
 * @since 2020-08-01
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ModelTemplateDto extends BaseDto {

    private static final long serialVersionUID = 1L;

        /**
        * 模型ID
        */
        private String modelId;


        /**
        * 站点ID
        */
        private String siteId;


        /**
        * 模板ID
        */
        private String templateId;


        /**
        * 内容静态化模板地址
        */
        private String templatePath;



}
