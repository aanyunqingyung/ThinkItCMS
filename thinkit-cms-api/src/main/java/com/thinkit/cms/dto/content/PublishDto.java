package com.thinkit.cms.dto.content;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class PublishDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "请选择记录!")
    public List<String> ids;

    public String status;

    public List<ContentDto> rows;

    public Date date;

}
