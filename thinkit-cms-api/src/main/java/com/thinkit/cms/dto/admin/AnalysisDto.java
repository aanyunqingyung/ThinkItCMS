package com.thinkit.cms.dto.admin;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 流量统计配置
 * </p>
 *
 * @author lg
 * @since 2020-11-25
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnalysisDto extends BaseDto {

        /**
        * 统计平台
        */
        private String analysisPlat;


        /**
        * pc端地址
        */
        private String pcUrl;


        /**
        * 手机端地址
        */
        private String mobileUrl;


        /**
        * 0:非默认 1：默认
        */
        private Integer isDefault;

}
