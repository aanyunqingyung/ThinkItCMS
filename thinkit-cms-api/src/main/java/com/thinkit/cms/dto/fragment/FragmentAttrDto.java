package com.thinkit.cms.dto.fragment;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.thinkit.utils.model.BaseDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 页面片段数据表
 * </p>
 *
 * @author lg
 * @since 2020-08-03
 */
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FragmentAttrDto extends BaseDto {

    /**
     * 封面图
     */
    private List<Map> cover =new ArrayList<>();


    /**
     * 片段路径
     */
    private String path;


    /**
     * 标题
     */
    private String title;


    /**
     * 描述
     */
    private String description;


    /**
     * 超链接
     */
    private String url;


    /**
     * 排序
     */
    private Integer sort;


    /**
     * 扩展数据data
     */
    private String data;

    /**
     * 模板id
     */
    private String templateId;


    private String fragmentModelId;

    /**
     * 0:未激活 1：已激活
     */
    private Boolean activate;

    /**
     * 参数所有
     */
    private Map<String, Object> params;
}
