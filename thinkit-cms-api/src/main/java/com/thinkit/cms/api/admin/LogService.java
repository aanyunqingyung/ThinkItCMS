package com.thinkit.cms.api.admin;//package com.thinkit.admin.service;
//
//import com.thinkit.admin.api.dto.LogDto;
//import com.thinkit.core.base.BaseService;
//
///**
// * <p>
// * 系统日志 服务类
// * </p>
// *
// * @author lgs
// * @since 2019-08-12
// */
//public interface LogService extends BaseService<LogDto> {
//
//
//    /**
//     * 创建操作日志记录
//     * @param pjp
//     * @param l
//     */
//    void saveLog(ProceedingJoinPoint pjp, int l);
//
//
//    /**
//     * 删除某个月之前的日志
//     * @param month
//     */
//    void deleteLogBeforeMonth(Integer month);
//
//
//}
