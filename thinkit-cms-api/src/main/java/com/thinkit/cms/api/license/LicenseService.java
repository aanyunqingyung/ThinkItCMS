package com.thinkit.cms.api.license;

import com.thinkit.cms.dto.license.LicenseDto;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.PageDto;
import org.springframework.web.multipart.MultipartFile;

public interface LicenseService {

    PageDto<LicenseDto> page();

    ApiResult importLicense(MultipartFile file);
}
