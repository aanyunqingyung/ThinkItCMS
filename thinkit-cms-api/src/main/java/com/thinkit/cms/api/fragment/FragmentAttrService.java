package com.thinkit.cms.api.fragment;
import com.thinkit.cms.dto.fragment.FragmentAttrDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.DynamicModel;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 页面片段数据表 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-03
 */
public interface FragmentAttrService extends BaseService<FragmentAttrDto> {


    /**
     * 插入页面片段数据
     * @param fragmentAttrDto
     */
    void insertFragmentAttr(FragmentAttrDto fragmentAttrDto);

    /**
     * 获取页面片段数据
     * @param id
     * @return
     */
    List<DynamicModel> getFragmentAttr(String id);

    Map<String, Object> getDesignAttrById(String id);

    /**
     * 修改页面片段
     * @param fragmentAttrDto
     */
    void updateFragmentAttr(FragmentAttrDto fragmentAttrDto);

    /**
     * 根据页面片段code 获取内容
     * @param code
     * @return
     */
    List<Map> listDataByCode(String code);
}