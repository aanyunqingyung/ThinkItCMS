package com.thinkit.cms.api.tag;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.core.base.BaseService;
import java.util.List;

/**
 * <p>
 * 标签 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-20
 */
public interface TagService extends BaseService<TagDto> {


    List<TagDto> listTags(List<String> asList, String siteId);

    List<String> listTags(List<String> tagIds);


    /**
     * 保存标签
     * @param tags
     * @return
     */
    List<String> saveTags(List<String> tags);
}