package com.thinkit.cms.api.template;
import com.thinkit.cms.dto.template.TemplateDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.ApiResult;
import com.thinkit.utils.model.TreeFileModel;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 模板表 服务类
 * </p>
 *
 * @author lg
 * @since 2020-07-23
 */
public interface TemplateService extends BaseService<TemplateDto> {


    /**
     * 保存模板记录
     * @param v
     */
    void save(TemplateDto v);

    /**
     * 更新模板记录
     * @param v
     */
    void update(TemplateDto v);


    /**
     * 根据模板id 加载模板文件
     * @param id
     * @return
     */
    List<TreeFileModel> loadTemplateTree(String id);

    /**
     * 获取文件内容
     * @param path
     * @return
     */
    ApiResult getFileContent(String path);

    /**
     * 设置文件内容
     * @param path
     * @param content
     * @return
     */
    boolean setFileContent(String path, String content);

    /**
     * 创建文件
     * @param path
     * @param fileName
     * @return
     */
    boolean createFile(String path, String fileName, Integer type);

    /**
     * 删除文件
     * @param path
     * @return
     */
    boolean delFile(String path);

    ApiResult checkerIsStartTemplate(String templateId);

    /**
     * 导入模板
     * @param files
     * @return
     */
    ApiResult uploadFile(List<MultipartFile> files);

    /**
     * 导出模板
     * @param tempId
     * @param response
     */
    void exportit(String tempId, HttpServletResponse response);

    ApiResult getDirects();

    List<TreeFileModel> loadPartFile(String id);

    /**
     * 导入文件
     * @param path
     * @param files
     * @return
     */
    ApiResult importFile(String path, List<MultipartFile> files) throws IOException;

    ApiResult unZipFile(String path);

    void downFiles(String path, HttpServletResponse response);
}