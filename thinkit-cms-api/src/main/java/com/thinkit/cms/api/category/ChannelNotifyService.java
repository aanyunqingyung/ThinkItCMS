package com.thinkit.cms.api.category;

public interface ChannelNotifyService {
    void notifyHomePage();


    void notifyWholeSite();

    /**
     * 通知文章同步到 solr 库
     */
    void notifyDbToNoSql(Integer pageNo,Integer pageSize,Integer index);

    void flushdb();
}
