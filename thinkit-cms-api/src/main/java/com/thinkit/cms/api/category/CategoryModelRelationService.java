package com.thinkit.cms.api.category;
import com.thinkit.cms.dto.category.CategoryModelRelationDto;
import com.thinkit.core.base.BaseService;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 分类选择模型时关系表 该模型值 tk_model 表 不是 分类模型表 服务类
 * </p>
 *
 * @author LG
 * @since 2020-08-10
 */
public interface CategoryModelRelationService extends BaseService<CategoryModelRelationDto> {


    void saveBatch(List<CategoryModelRelationDto> vs);

    String loadTemplatePath(@Param("params") Map<String, Object> params);

    /**
     * 查询栏目对应模型的后选择的模板
     * @param categoryId
     * @return
     */
    String getTempPathByCategoryId(String categoryId, String modelId);
}