package com.thinkit.cms.api.category;

import com.thinkit.cms.dto.category.CategoryAttrDto;
import com.thinkit.core.base.BaseService;

/**
 * <p>
 * 分类扩展 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-10
 */
public interface CategoryAttrService extends BaseService<CategoryAttrDto> {

    void updateSeo(CategoryAttrDto v);

    void updateByCategory(String id, String attrData);

    CategoryAttrDto getSeo(String categoryId);
}