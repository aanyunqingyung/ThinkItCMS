package com.thinkit.cms.api.content;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.content.ContentAnalysisDto;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.content.PublishDto;
import com.thinkit.cms.dto.tag.TagDto;
import com.thinkit.core.base.BaseService;
import com.thinkit.utils.model.*;
import org.apache.solr.common.SolrDocument;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 内容 服务类
 * </p>
 *
 * @author lg
 * @since 2020-08-15
 */
public interface ContentService extends BaseService<ContentDto> {


    IPage<ContentDto>   pageContentForNoSql(Integer pageNo, Integer pageSize);

    Tree<CategoryDto> treeCategory();

    List<DynamicModel> getFormDesign(String modelId);

    void save(ContentDto v);

    Map<String,Object> getDetail(String id);

    void update(ContentDto v);

    boolean delete(String id, boolean realDel);

    void deletes(List<String> ids, boolean realDel);

    /**
     * 根據内容ID 加載 内容詳情
     * @param params
     * @return
     */
    Map<String, Object> loadTempParams(Map<String, Object> params, List<String> status);

    /**
     * 根據内容ID 加載 内容詳情
     * @param params
     * @return
     */
    void loadTempParams(List<Map<String, Object>> params, List<String> status);

    /**
     * 置顶
     * @param contentDto
     * @return
     */
    void top(ContentDto contentDto);

    Set<String> getTopTag();

    void publish(PublishDto publishDto, boolean byJob);

    /**
     * 加载置顶栏目内容列表
     * @return
     */
    Map<String, Object> listContent(Map<String, Object> params, Boolean notify);



    /**
     * 获取指定栏目编码的内容
     * @param code
     * @param num
     * @param order
     * @return
     */
    List<Map<String, Object>> listByCode(String code, String categoryId, Integer num, String order);

    /**
     * 查询内容
     * @param id
     * @return
     */
    Map<String, Object> contentById(String id);

    /**
     * 查询上一篇或者下一篇
     * @param id
     * @return
     */
    Map<String, Object> nextPrevious(String id, String categoryId);


    /**
     * 查询内容所有参数用于生成全部内容
     * @return
     */
    Map<String,Object> pageContentParamsForAllGen(Integer pageNo, Integer pageSize);

    /**
     * 获取单页栏目的内容
     * @param categoryId
     * @param code
     * @return
     */
    Map<String, Object> categorySingleData(String categoryId, String code);

    /**
     * 根据文章id 查询文章所在的分类
     * @param ids
     * @return
     */
    Set<String> listCategoryByCids(List<String> ids);

    /**
     * 获取
     * @param categoryId
     * @param code
     * @param where
     * @return
     */
    Map<String, List> getTopNews(String categoryId, String code, String where);

    /**
     * 获取
     * @param categoryId
     * @param code
     * @param where
     * @return
     */
    Map<String, List> getTopTags(String categoryId, String code, String where);

    /**
     * 重置内容模型
     */
    void resetModelId(String categoryId);

    /**
     * p批量生成静态页
     * @param ids
     */
    void reStaticBatchGenCid(List<String> ids);

    PageDto<ContentDto> pageRecycler(PageDto<ContentDto> pageDto);

    void recyclerByPks(List<String> ids);

    ApiResult viewContent(String id);

    ApiResult move(String categoryId, List<String> contentIds);

    void jobPublish(List<String> ids, Date date);

    String queryCategoryId(String contentId);

    Map<String,Object> nextPrevious(String id,Boolean isNext,String categoryId);

    void deleteAllByCategoryId(String id, List<String> contentIds);

    /**
     * 按照月份统计发帖排行
     * @param contentAnalysisDto
     * @return
     */
   Map<String,Object> analysisMonthTop(ContentAnalysisDto contentAnalysisDto);

    /**
     * 点击次数
     * @param cid
     * @return
     */
    Long viewClicks(String cid);

    ApiResult viewLikes(String cid, String userAgent);

   void syncViewLikes();

    PageDto<SolrDocument> searchKeyWord(PageDto<SolrSearchModel> pageDto);

    List<ContentDto> listContents(List<String> noStoreIds,String status);

    Map<String, Object> wechatArticle(String url,String modelId,Integer type,String cookie);

    List<Map<String, Object>> listUpToDate(String code, String categoryId, Integer num, String order,Boolean publish);
}